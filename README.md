# isms_web_robotu



iSMS Web Robotu



# Servis Desk
-------------
Programda oluşan herhangi bir hata durumunda incoming+electrocoder/isms_web_robotu@incoming.gitlab.com mail adresine mail gönderiniz.



# Kod List Değişiklikler

v4.4 14.04.2019
---------------
Hızlandırıldı,
Db ye kayıt hemen yapılmaya başlandı,
Siparis tarihine gore siralandı,
500+ sipariş sayı problemi çözüldü 

v4 20.09.2018
-------------
Firebird veritabanı kullanımına geçildi.
Sayfalama problem giderildi.

v3.1 18.09.2018
---------------
507 : td hata kodu eklendi.

v3 17.09.2018
-------------
log_file.write düzenlendi.

v2 16.09.2018
-------------
print("diger sayfa") iyileştirmeleri yapıldı.

v1 13.09.2018
-------------
time.sleep ayarları yapıldı.



# Kod List TO CSV Değişiklikler

v4.1 11.10.2018
---------------
Musteri isminde tab karakteri duzeltildi

v4 20.09.2018
-------------
Firebird veritabanı kullanımına geçildi.

v3 17.09.2018
-------------
log_file.close eklendi.

v2 16.09.2018
-------------
bugun=True parametresi eklendi.

v1 13.09.2018
-------------
time.sleep ayarları yapıldı.



# Cari Değişiklikler

v1 21.09.2018
-------------
Cari projesi baslatildi.

